<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getUser()
    {
        return auth()->user();
    }

    public function updateProfile(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = auth()->user();

        $user->name = $request->name;

        $user->save();

        return $user;
    }

    /**
     * Get the login validation rules.
     *
     * @param  array
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:50'],
        ]);
    }
}
