#Laravel + Vue Single Page Application


```sh 
    git clone https://github.com/wanyay/laravue.git
    
    cd laravue
    
    composer install
    
    npm install
    
    cp .env.example .env
    
    php artisan key:generate
    
    php artisan passport:install
    
    npm run dev
    
    php artisan serve
```
