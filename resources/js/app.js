/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("./bootstrap");

import "bootstrap/dist/css/bootstrap.css";
import Vue from "vue";
import App from "./App.vue";
import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import router from "./router/routes";
import { store } from "./vuex/store";
import "element-ui/lib/theme-chalk/index.css";

Vue.use(ElementUI, { locale });

new Vue({
  created() {
    if (this.$store.getters.isAuthenticated) {
      this.$store.dispatch("userRequest");
    }
  },
  router,
  store,
  render: h => h(App)
}).$mount("#app");
