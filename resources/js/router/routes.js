import Vue from "vue";
import VueRouter from "vue-router";
import { store } from "../vuex/store";
import Login from "./../components/auth/Login";
import AdminLayout from "./../components/layout/AdminLayout";
import Dashboard from "./../components/dashboard/Dashboard";
import Profile from "../components/profile/Profile";
import Register from "../components/auth/Signup";
import NotFound from "../components/pages/NotFound";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/",
    name: "layout",
    component: AdminLayout,
    redirect: "/dashboard",
    meta: { requiresAuth: true },
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: Dashboard
      },
      {
        path: "/profile",
        name: "Profile",
        component: Profile
      }
    ]
  },
  {
    path: "/Register",
    name: "Register",
    component: Register
  },
  {
    path: "*",
    name: "404",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  if (
    to.matched.some(record => record.meta.requiresAuth) &&
    !store.getters.isAuthenticated
  ) {
    // if route requires auth and user isn't authenticated
    next("/login");
  } else {
    next();
  }
});

export default router;
