import Vue from "vue";
import Errors from "./../../error";

const state = {
  status: "",
  profile: {},
  errors: new Errors()
};

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile.name
};

const actions = {
  userRequest: ({ commit, dispatch }) => {
    commit("userRequest");
    axios
      .get("/user")
      .then(resp => {
        commit("userSuccess", resp.data);
      })
      .catch(err => {
        commit("userError", err.response.data);
        // if resp is unauthorized, logout, to
        dispatch("authLogout");
      });
  },

  updateProfile: ({ commit, dispatch }, payload) => {
    commit("userRequest");
    return new Promise((resolve, reject) => {
      axios
        .put("/user", payload)
        .then(resp => {
          commit("userSuccess", resp.data);
        })
        .catch(err => {
          commit("userError", err.response.data);
        });
    });
  }
};

const mutations = {
  userRequest: state => {
    state.status = "loading";
  },
  userSuccess: (state, resp) => {
    state.status = "success";
    Vue.set(state, "profile", resp);
  },
  userError: (state, err) => {
    state.status = "error";
    let errors = err.errors ? err.errors : {};
    state.errors.record(errors);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
